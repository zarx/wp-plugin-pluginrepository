<?php
	if ($_GET['id']) {
		$repo = get_post($_GET['id']);
		$versions = get_post_meta($repo->ID, 'pr_versions');
		$versions = $versions[0];
		if ($_GET['del'] && isset($versions[$_GET['del']])) {
			unset($versions[$_GET['del']]);
			add_post_meta( $repo->ID, 'pr_versions', $versions, true ) || update_post_meta( $repo->ID, 'pr_versions', $versions );
		}
		
		if ($_POST['action'] == 'add') {
			$version = $_POST['v1'].'.'.$_POST['v2'].'.'.$_POST['v3'];
			$filename = $repo->post_name . '_' . $version . '.zip';
			$filepath = plugin_dir_path( __FILE__ ) . 'files/' . $filename;
			copy($_FILES['file']['tmp_name'], $filepath);
			
			$versions[$version] = array(
				'version' => $version, //Current version available
				'date' => date('Y-m-d'), //Date version was released
				'author' => 'Johan Boström', //Author name - can be linked using html - <a href="http://link-to-site.com">Author Name</a>
				'requires' => '2.8', // WP version required for plugin
				'tested' => '3.0.1', // WP version tested with
				'homepage' => 'http://www.sigward.me', // Site devoted to your plugin if available
				'downloaded' => '1000', // Number of times downloaded
				'external' => '', // Unused
				//plugin.zip is the same as file_name
				//'package' => 'download.php?key=' . md5($filename . mktime(0,0,0,date("m"),date("d"),date("Y"))),
				//file_name is the name of the file in the update folder.
				'file_name' => $filename,
				'sections' => array(
					/* Plugin Info sections tabs.  Each key will be used as the title of the tab, value is the contents of tab.
					  Must be lowercase to function properly
					  HTML can be used in all sections below for formating.  Must be properly escaped ie a single quote would have to be \'
					  Screenshot section must use exteranl links for img tags.
					 */
					'description' => $_POST['desc'], //Description Tab
					'installation' => 'Install Info', //Installaion Tab
					'screen shots' => 'Screen Shots', //Screen Shots
					'change log' => 'Change log', //Change Log Tab
					'faq' => 'FAQ', //FAQ Tab
					'other notes' => 'Other Notes'    //Other Notes Tab
				)
			);
			
			add_post_meta( $repo->ID, 'pr_versions', $versions, true ) || update_post_meta( $repo->ID, 'pr_versions', $versions );
		}
		
		
		uksort($versions, 'version_compare');
		$versions = array_reverse($versions);
	}

?>

<h2>Repository</h2>
<form method="get">
	ID: <input type="number" name="id" value="<?= $_GET['id'] ?>" />
	<input type="hidden" name="page" value="<?= $_GET['page'] ?>" />
	<input type="submit" />
</form>

<h2>Add version</h2>
<form method="post" enctype="multipart/form-data" action="?page=<?=$_GET['page']?>&id=<?=$repo->ID?>">
	Version: <input type="number" style="width: 40px;" name="v1" size="3" value="0" />.<input type="number" style="width: 40px;" name="v2" size="3" value="0" />.<input type="number" style="width: 40px;" name="v3" size="3" value="0" />
	<br/>
	<textarea name="desc"></textarea><br/>
	<input type="file" name="file" /><br/>
	<input type="hidden" name="action" value="add" />
	<input type="submit" />
</form>

<h2>Versions</h2>
<table class="wp-list-table widefat fixed posts">
	<thead>
	<tr>
		<th scope="col" id="title" class="manage-column column-title sortable desc" colspan="1" style="">
			<a>
				<span>Version</span>
			</a>
		</th>
		<th scope="col" id="title" class="manage-column column-title sortable desc" colspan="1" style="">
			<a>
				<span>File</span>
			</a>
		</th>
		<th scope="col" id="title" class="manage-column column-title sortable desc" colspan="1" style="">
			<a>
				<span>Description</span>
			</a>
		</th>
	</tr>
	</thead>

	<tfoot>
	<tr>
	</tr>
	</tfoot>

	<tbody id="the-list">
		<?php foreach ($versions as $key => $v): ?>
		<tr class="no-items">
			<td class="colspanchange">
				<?= $v['version'] ?>
			</td>
			<td class="colspanchange">
				<?= $v['file_name'] ?>
			</td>
			<td class="colspanchange">
				<?= $v['sections']['description'] ?>
			</td>
			<td class="colspanchange">
				<a href="?page=<?=$_GET['page']?>&id=<?=$_GET['id']?>&del=<?= $key ?>">Del</a>
			</td>
		</tr>	
		<?php endforeach; ?>
	</tbody>
</table>

