<?php

$versions = get_post_meta($post->ID, 'pr_versions');
$versions = $versions[0];
uksort($versions, 'version_compare');
$versions = array_reverse($versions);

$packages = array(
	'versions' => array(),
    'info' => array(
        'url' => 'http://your_plugin_webiste'  // Site devoted to your plugin if available
    ),
);
foreach ($versions as $vKey => $v) {
	$v['package'] = get_permalink() . "?download=" . $vKey;
	$packages['versions'][$vKey] = $v;
}