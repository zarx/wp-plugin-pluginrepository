<?php
/*
Plugin Name: Plugin Repository
Plugin URI: http://www.sigward.me
Description: Handles plugin updates
Version: 1.0
Author: Johan Boström
Author URI: www.sigward.me
*/

DEFINE('PR_DIR', dirname( __FILE__ ));

add_action( 'init', 'pr_create_post_type' );
function pr_create_post_type() {
	register_post_type( 'pr_repository',
		array(
			'labels' => array(
				'name' => __( 'Repositories' ),
				'singular_name' => __( 'Repository' )
			),
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'public'			 => true,
			'rewrite'            => array( 'slug' => 'repo' ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title' )
		)
	);
}

function pr_register_templates($single_template) {
     global $post;

     if ($post->post_type == 'pr_repository') {
          $single_template = dirname( __FILE__ ) . '/single-pr_repository.php';
     }
     return $single_template;
}
add_filter( 'single_template', 'pr_register_templates' );

function pr_repository_save( $post_id ) {
	if ( wp_is_post_revision( $post_id ) )
		return;
	
	$post = get_post($post_id);
	switch($post->post_type) {
		case 'pr_repository':
			//update_post_meta($post_id, '');
			break;
	}
}
add_action( 'save_post', 'pr_repository_save' );

function pr_menu() {
    $name = 'Repositories';
    $slug = 'pr_repositories';
    add_menu_page($name, $name, 'manage_options', 'PluginRepository/projects-admin.php', null, null, null);
    add_submenu_page('PluginRepository/projects-admin.php', 'Project', 'Project', 'manage_options', 'PluginRepository/project-admin.php');
}
add_action("admin_menu", "pr_menu");  