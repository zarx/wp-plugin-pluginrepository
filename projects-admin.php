<?php
	//if ($_POST[''])

	$repositories = get_posts(array(
		'post_type' => 'pr_repository'
	));
?>

<h2>Add repository</h2>
<form method="post">
	Name: <input name="name" />
	Slug: <input name="slug" />
</form>

<h2>Repositories</h2>
<table class="wp-list-table widefat fixed posts">
	<thead>
	<tr>
		<th scope="col" id="title" class="manage-column column-title sortable desc" colspan="1" style="">
			<a>
				<span>Repository</span>
			</a>
		</th>
		<th scope="col" id="title" class="manage-column column-title sortable desc" colspan="1" style="">
			<a>
				<span>Slug</span>
			</a>
		</th>
		<th scope="col" id="title" class="manage-column column-title sortable desc" colspan="1" style="">
			<a>
				<span></span>
			</a>
		</th>
	</tr>
	</thead>

	<tfoot>
	<tr>
	</tr>
	</tfoot>

	<tbody id="the-list">
		<?php foreach ($repositories as $repo): ?>
		<tr class="no-items">
			<td class="colspanchange">
				<a href="/wp-admin/post.php?post=<?= $repo->ID ?>&action=edit"><?= $repo->post_title ?></a>
			</td>
			<td class="colspanchange">
				<?= $repo->post_name ?>
			</td>
			<td class="colspanchange">
				<a href="/wp-admin/admin.php?page=PluginRepository/project-admin.php&id=<?= $repo->ID ?>">Versions</a>
			</td>
			<td class="colspanchange">
				<a href="<?=get_permalink($repo->ID)?>"><?=get_permalink($repo->ID)?></a>
			</td>
		</tr>	
		<?php endforeach; ?>
	</tbody>
</table>

